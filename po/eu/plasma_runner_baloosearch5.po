# Translation for plasma_runner_baloosearch5.po to Euskara/Basque (eu).
# Copyright (C) 2018, Free Software Foundation.
# Copyright (C) 2019-2022, This file is copyright:
# This file is distributed under the same license as the kde-workspace package.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2018, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-05 00:46+0000\n"
"PO-Revision-Date: 2022-08-12 17:53+0200\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.04.3\n"

#: baloosearchrunner.cpp:65
#, kde-format
msgid "Open Containing Folder"
msgstr "Ireki hau barnean duen karpeta"

#: baloosearchrunner.cpp:87
#, kde-format
msgid "Audio"
msgstr "Audioa"

#: baloosearchrunner.cpp:88
#, kde-format
msgid "Image"
msgstr "Irudia"

#: baloosearchrunner.cpp:89
#, kde-format
msgid "Video"
msgstr "Bideoa"

#: baloosearchrunner.cpp:90
#, kde-format
msgid "Spreadsheet"
msgstr "Kalkulu-orria"

#: baloosearchrunner.cpp:91
#, kde-format
msgid "Presentation"
msgstr "Aurkezpena"

#: baloosearchrunner.cpp:92
#, kde-format
msgid "Folder"
msgstr "Karpeta"

#: baloosearchrunner.cpp:93
#, kde-format
msgid "Document"
msgstr "Dokumentua"

#: baloosearchrunner.cpp:94
#, kde-format
msgid "Archive"
msgstr "Fitxategia"

#: baloosearchrunner.cpp:95
#, kde-format
msgid "Text"
msgstr "Testua"
